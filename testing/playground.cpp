#include "../../include/libVector.hpp"
#include <stdio.h>

// #define GLEW_STATIC
// #include <GL/glew.h>
// #include <GLFW/glfw3.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int N;
    double epsilon;
    FILE *parameters;

    parameters = fopen("testing.txt", "r");
    fscanf(parameters, "%d %lf", &N, &epsilon);
    fclose(parameters);

    printf("%d %f\n", N, 1 / epsilon);

    int i, j;
    srand(time(0));

    i = rand();
    j = rand();
    printf("%d, %d\n", i, j);
}
/*
Created by Nick Crane
Created: 07/16/20
Edited: 07/16/20

Notes: Contains function prototypes for 
particle sim.
*/

#pragma once

#include "libParticle.hpp"

// Initialize particles, box, and parameters:
void SetupParticles(Particle *pArray, const int Nx, const int Ny, const int Nz, const double density, const double temp);
void SetupParameters(); // might not need

// Single step in the simulation:
void SingleStep(Particle *pArray, const int N, const double dt, const double epsilon, const double sigma, const vec3d L);
void ComputeForces(Particle *pArray, const int N, const double dt, const double epsilon, const double sigma);
void UpdatePositions(Particle *pArray, const int N, const double dt);
void UpdateVelocities(Particle *pArray, const int N, const double dt);
void EvaluateProperties(Particle *pArray, const int N, const double dt);
void ApplyBoundaryConditions(Particle *pArray, const int N, const double dt, const vec3d L);
double Dabs(double x);
int SanityCheck(Particle *pArray, const int N, const vec3d L);

// Draw whenever Jim finishes whatever tf he's doing
void DrawParticles(Particle *pArray, const int N);
/*
Created by Nick Crane
Created: 07/15/20
Edited: 07/15/20

Notes: Contains two basic classes with functions
and operator overloading to allow for simple and
intuitive vector math. Vectors are strictly in 
cartesian coordinates (for now).
*/

#pragma once
#include <math.h> // need the square root function

class vec2d 
{
    public:
        double i, j;

        vec2d()
        {
            i = 0.0;
            j = 0.0;
        }

        vec2d(double x, double y)
        {
            i = x;
            j = y;
        }

        ~vec2d() {}

        // vector properties
        inline double mag2() const
        {
            return i*i + j*j;
        }

        inline double mag() const
        {
            return sqrt(this->mag2());
        }

        // addition of two vectors
        inline vec2d operator+(const vec2d& other) const
        {
           return vec2d(i + other.i, j + other.j);
        }

        inline void operator+=(const vec2d& other)
        {
            i += other.i;
            j += other.j;
        }

        // subtraction of two vectors
        inline vec2d operator-(const vec2d& other) const 
        {
           return vec2d(i - other.i, j - other.j);
        }

        inline void operator-=(const vec2d& other)
        {
            i -= other.i;
            j -= other.j;
        }

        // scalar factors
        inline vec2d operator*(const double& s) const
        {
           return vec2d(s * i, s * j);
        }

        inline void operator*=(const double& s)
        {
            i *= s;
            j *= s;
        }
        
        // linear algebra tools
        inline double operator*(const vec2d& other) const
        {
            return i*other.i + j*other.j;
        }

        inline vec2d unit() const
        {
            return vec2d(i, j) * (1 / (this->mag()));
        }

        inline void normalize()
        {
            double len = this->mag();
            i /= len;
            j /= len;
        }
};

class vec3d 
{
    public:
        double i, j, k;

        vec3d()
        {
            i = 0.0;
            j = 0.0;
            k = 0.0;
        }

        vec3d(double x, double y, double z)
        {
            i = x;
            j = y;
            k = z;
        }

        ~vec3d() {}

        // vector properties
        inline double mag2() const 
        {
            return i*i + j*j + k*k;
        }

        inline double mag() const
        {
            return sqrt(this->mag2());
        }

        // addition of two vectors
        inline vec3d operator+(const vec3d& other) const
        {
           return vec3d(i + other.i, j + other.j, k + other.k);
        }

        inline void operator+=(const vec3d& other)
        {
            i += other.i;
            j += other.j;
            k += other.k;
        }

        // subtraction of two vectors
        inline vec3d operator-(const vec3d& other) const
        {
           return vec3d(i - other.i, j - other.j, k - other.k);
        }

        inline void operator-=(const vec3d& other)
        {
            i -= other.i;
            j -= other.j;
            k -= other.k;
        }

        // scalar factors
        inline vec3d operator*(const double& s) const
        {
           return vec3d(s * i, s * j, s * k);
        }

        inline void operator*=(const double& s)
        {
            i *= s;
            j *= s;
            k *= s;
        }
        
        // linear algebra tools
        inline double operator*(const vec3d& other) const // dot product
        {
            return i*other.i + j*other.j + k*other.k;
        }

        inline vec3d operator%(const vec3d& other) const // cross product
        {
            return vec3d(j*other.k - k*other.j, k*other.i - i*other.k, i*other.j - j*other.i);
        }

        inline vec3d unit() const // returns a unit vector with same direction
        {
            return vec3d(i, j, k) * (1 / (this->mag()));
        }

        inline void normalize() // normalizes the vector
        {
            double len = this->mag();
            i /= len;
            j /= len;
            k /= len;
        }
};
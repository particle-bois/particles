/*
Created by Nick Crane
Created: 07/16/20
Edited: 07/16/20

Notes: Contains the classes for basic particles.
*/

#pragma once

#include "libVector.hpp"
#include "libPhysicsConstants.hpp"

class Particle 
{
    public:
        vec3d s, v, a;
        double m;

        Particle()
        {
            s = v = a = vec3d();
            m = 1;
        }

        Particle(vec3d pos, vec3d vel, vec3d acc, double mass)
        {
            s = pos;
            v = vel;
            a = acc;
            m = mass;
        }

        ~Particle() {}

        // basic properties of a particle
        inline double p()
        {
            return m * v.mag();
        }

        inline double ke()
        {
            return 0.5 * m * v.mag2();
        }
};

class Electron : public Particle
{
    public:
        double m = electron_m, q = -elementary_q;

        Electron()
        {
            s = v = a = vec3d();
        }

        Electron(vec3d pos, vec3d vel, vec3d acc)
        {
            s = pos; v = vel; a = acc;
        }

        ~Electron() {}
};
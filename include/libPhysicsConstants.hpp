/*
Created by Nick Crane
Created: 07/16/20
Edited: 07/16/20

Notes: Contains many commonly used physics 
constants. All constants are in SI units.
*/

#pragma once

#define Bolt_k 1.38064852e-23 // m2 kg s-2 K-1 // Boltzmann Constant
#define elementary_q 1.60217662e-19 // C // elementary charge
#define electron_m 9.10938356e-31 // kg // mass of electron
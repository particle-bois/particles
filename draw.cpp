/*
Created by Nick Crane
Created: 07/26/20
Edited: 07/26/20

Notes: Uses Jim's anim2 to draw the 
particles
*/

#include "include/Particles.hpp"
#include "include/Anim.h"

void DrawParticles(Particle* const pArray, const int N)
{
    int i;
    static float pos[3];
    static float color[3] = {1.0f, 1.0f, 1.0f};
    static const float scale = 1;

    for (i = 0; i < N; i++)
    {
        AnimDrawSphere(pArray[i].s.i, pArray[i].s.j, pArray[i].s.k, color, scale);
    }
    AnimEndFrame();
}
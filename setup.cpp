/*
Created by Nick Crane
Created: 07/17/20
Edited: 07/17/20

Notes: Sets up the initial conditions for
the simulation.
*/

#include "include/Particles.hpp"
#include <stdlib.h> // need rand to initialize random velocities
#include <time.h>

// Sets initial condition for particles
void SetupParticles(Particle* const pArray, const int Nx, const int Ny, const int Nz, const double density, const double temp)
{
    int i, j, k, index;
    // static const int N = Nx * Ny * Nz;

    // we want particles to be centered about the origin
    static const vec3d offset = vec3d((Nx - 1) / density, (Ny - 1) / density, (Nz - 1) / density) * 0.5; 

    srand(time(0));

    for (i = 0; i < Nx; i++)
        for (j = 0; j < Ny; j++)
            for (k = 0; k < Nz; k++)
            {
                index = i * Ny * Nz + j * Nz + k; // current particle index
                pArray[index].s = vec3d(i / density, j / density, k / density) - offset;
                pArray[index].v.i = ((double) rand() / RAND_MAX - 0.5);
                pArray[index].v.j = ((double) rand() / RAND_MAX - 0.5);
                pArray[index].v.k = ((double) rand() / RAND_MAX - 0.5);
                pArray[index].v.normalize();
                pArray[index].v *= temp;
                pArray[index].a = vec3d();
            }
}

// I don't know what I need this for :)
void SetupParameters()
{

}
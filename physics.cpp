/*
Created by Nick Crane
Created: 07/17/20
Edited: 07/17/20

Notes: Contains the functions pertaining to the 
physics of the simulation.
*/

#include "include/Particles.hpp"

inline double Dabs(double x)
{
    if (x > 0)
        return x;
    return -x;  
}

void SingleStep(Particle* const pArray, const int N, const double dt, const double epsilon, const double sigma, const vec3d L)
{
    UpdatePositions(pArray, N, dt);
    ComputeForces(pArray, N, dt, epsilon, sigma);
    UpdateVelocities(pArray, N, dt);
    ApplyBoundaryConditions(pArray, N, dt, L);
    UpdatePositions(pArray, N, dt);
}

void ComputeForces(Particle* const pArray, const int N, const double dt, const double epsilon, const double sigma)
{
    vec3d force, u; // u is vector between two particles positions
    double umag, umag2, umag8, umag14;
    int i, j;

    static const double rm = pow(2.0, 1.0 / 6.0 * sigma); // determines whether force should be computed
    static const double rrm = rm * rm;

    static const double sigma2 = sigma * sigma;
    static const double sigma6 = sigma2 * sigma2 * sigma2;
    static const double sigma12 = sigma6 * sigma6;

    for (i = 0; i < (N - 1); i++)
    {
        for (j = (i + 1); j < N; j++)
        {
            u = pArray[i].s - pArray[j].s;

            if (u.mag2() < rrm)
            {
                umag = u.mag();
                umag2 = umag * umag;
                umag8 = umag2 * umag2 * umag2;
                umag14 = umag8 * umag8 / umag2;
                force = u * (48 * epsilon * sigma12 / umag14 - 24 * epsilon * sigma6 / umag8);
                pArray[i].a += force * (1 / pArray[i].m);
                pArray[j].a -= force * (1 / pArray[j].m);
            }
        }
    }
}

inline void UpdateVelocities(Particle* const pArray, const int N, const double dt)
{
    int i;

    for (i = 0; i < N; i++)
    {
        pArray[i].v += pArray[i].a * dt;
        pArray[i].a = vec3d();
    }
}

// Uses leapfrog update technique
inline void UpdatePositions(Particle* const pArray, const int N, const double dt)
{
    int i;

    for (i = 0; i < N; i++)
        pArray[i].s += pArray[i].v * (dt * 0.5);
}

// If particle hits wall, bounce
void ApplyBoundaryConditions(Particle* const pArray, const int N, const double dt, const vec3d L)
{
    int i;
    static const double lx = L.i;
    static const double ly = L.j;
    static const double lz = L.k;

    for (i = 0; i < N; i++)
    {
        if (Dabs(pArray[i].s.i + pArray[i].v.i * dt) > (lx / 2))
        {
            pArray[i].v.i *= -1;
        }

        if (Dabs(pArray[i].s.j + pArray[i].v.j * dt) > (ly / 2))
        {
            pArray[i].v.j *= -1;
        }

        if (Dabs(pArray[i].s.k + pArray[i].v.k * dt) > (lz / 2))
        {
            pArray[i].v.k *= -1;
        }

        /*
        pArray[i].v.i *= -1 * (Dabs(pArray[i].s.i + pArray[i].v.i * dt) > lx / 2);
        pArray[i].v.j *= -1 * (Dabs(pArray[i].s.j + pArray[i].v.j * dt) > ly / 2);
        pArray[i].v.k *= -1 * (Dabs(pArray[i].s.k + pArray[i].v.k * dt) > lz / 2);
        */
    }
}

int SanityCheck(Particle* const pArray, const int N, const vec3d L)
{
    int i;
    static const double lx = L.i;
    static const double ly = L.j;
    static const double lz = L.k;

    for (i = 0; i < N; i++)
    {
        if (pArray[i].s.i > (lx / 2) || pArray[i].s.j > (ly / 2) || pArray[i].s.k > (lz / 2))
        {
            return 1;
        }
    }

    return 0;
}
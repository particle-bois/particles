/*
Created by Nick Crane
Created: 07/16/20
Edited: 07/16/20

Notes: Simulates a gas in a box. Forces approximated
using Van der Waals potential. No other forces included.
*/

#include "include/Particles.hpp"
#include <stdio.h>

#define ANIM 1

int main()
{
    static double dt, t = 0;
    static int maxSteps, incSteps, step = 0;
    static int Nx, Ny, Nz;
    static double density, temperature;
    static double sigma, epsilon;
    FILE *parameters;

    parameters = fopen("parameters.txt", "r");
    fscanf(parameters, "%lf %d %d %d %d %d %lf %lf %lf %lf", &dt, &maxSteps, &incSteps, &Nx, &Ny, &Nz, &density, &temperature, &sigma, &epsilon);
    fclose(parameters);

    static const int N = Nx * Ny * Nz;
    Particle* const particles = new Particle[N];

    vec3d L = vec3d(Nx / density + 1.0, Ny / density + 1.0, Nz / density + 1.0);

    SetupParticles(particles, Nx, Ny, Nz, density, temperature);
    #if ANIM == 1
        DrawParticles(particles, N);
    #endif


    /* --- MAIN LOOP --- */

    printf("Beginning main loop\n");
    while (step < maxSteps)
    {
        SingleStep(particles, N, dt, epsilon, sigma, L);

        if (step % incSteps == 0)
        {
            if (SanityCheck(particles, N, L) == 1)
            {
                fprintf(stderr, "Sanity check failed at step: %d\n", step);
                return 1;
            }

            #if ANIM == 1
                if (step % incSteps == 0)
                {
                    DrawParticles(particles, N);
                }
            #endif
        }

        t += dt;
        step++;
    }

    printf("Exited main loop successfully\n");
    delete[] particles;
    return 0;
}